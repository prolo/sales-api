package br.edu.unisep.sales.controller;

import br.edu.unisep.sales.domain.dto.SaleDto;
import br.edu.unisep.sales.domain.usecase.FindAllSalesUseCase;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/sale")
public class SalesController {

    private FindAllSalesUseCase findAllProductsUseCase;

    @GetMapping
    public ResponseEntity<List<SaleDto>> findAll() {
        var products = findAllProductsUseCase.execute();
        return ResponseEntity.ok(products);
    }

}

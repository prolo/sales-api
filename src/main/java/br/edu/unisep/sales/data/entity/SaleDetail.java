package br.edu.unisep.sales.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sale_details")
public class SaleDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "product_offer")
    private Double productOffer;

    @Column(name = "quantity")
    private Integer quantity;

}

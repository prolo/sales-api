package br.edu.unisep.sales.data.repository;

import br.edu.unisep.sales.data.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleRepository extends JpaRepository<Sale, Integer> {
}

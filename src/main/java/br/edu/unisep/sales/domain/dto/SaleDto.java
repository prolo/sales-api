package br.edu.unisep.sales.domain.dto;

import br.edu.unisep.sales.data.entity.PaymentMethod;
import br.edu.unisep.sales.data.entity.SaleDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class SaleDto {

    private Integer id;
    private Integer customer_id;
    private Integer address_id;
    private Double sale_offer;
    private PaymentMethod paymentMethod;
    private Double total;
    private List<SaleDetail> saleDetails;

}

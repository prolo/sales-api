package br.edu.unisep.sales.domain.builder;

import br.edu.unisep.sales.data.entity.Sale;
import br.edu.unisep.sales.domain.dto.SaleDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SaleBuilder {

    public List<SaleDto> from(List<Sale> sales) { return sales.stream().map(this::from).collect(Collectors.toList()); }

    public SaleDto from(Sale sales) {
        return new SaleDto(
                sales.getId(),
                sales.getCustomerId(),
                sales.getAddressId(),
                sales.getSaleOffer(),
                sales.getPaymentMethod(),
                sales.getTotal(),
                sales.getSaleDetails()
        );
    }
}

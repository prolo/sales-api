package br.edu.unisep.sales.domain.usecase;

import br.edu.unisep.sales.data.repository.SaleRepository;
import br.edu.unisep.sales.domain.builder.SaleBuilder;
import br.edu.unisep.sales.domain.dto.SaleDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllSalesUseCase {

    private final SaleRepository saleRepository;
    private final SaleBuilder saleBuilder;

    public List<SaleDto> execute() {
        var sales = saleRepository.findAll();

        return saleBuilder.from(sales);
    }
}
